module.exports = config => {
  const fs       = require('fs')
  const path     = require('path')
  const mongoose = require('mongoose')
  const basename  = path.basename(module.filename)
  const models = {}
  
  mongoose.Promise = global.Promise
  mongoose.connect(config.mongo)
  const db = mongoose.connection
  db.on('error', console.error.bind(console, 'connection error:'))
  
  return new Promise((resolve, reject) => {
    db.once('open', () => {
      fs
      .readdirSync(__dirname)
      .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
      })
      .forEach(file => {
        const model = require(path.join(__dirname, file))(mongoose)
        models[model.modelName] = model
      })
      
      resolve(models)
    })
  })
}
