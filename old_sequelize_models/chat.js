'use strict';
module.exports = function(sequelize, DataTypes) {
  var Chat = sequelize.define('Chat', {
    name: DataTypes.STRING,
    external_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    hooks: {
      beforeCreate: function (chat, options, fn) {
        var d = new Date();
        chat.createdAt = d;
        chat.updatedAt = d;
        fn(null, chat);
      },
      beforeUpdate: function (chat, options, fn) {
        chat.updatedAt = new Date();
        fn(null, chat);
      }
    }
  });
  return Chat;
};
